# <img src="logo.png" width=48 height=48 /> SuperSmartMirror

A mirror displaying all the information you need.

## Hardware

 * Raspberry Pi 3B
 * An old Screen
 * HDMI -> VGA converter
 * Self-built woden frame
 * Glas/Plexiglas
 * Spy foil
 
## Software
 * mirr.OS
 * OpenWeatherMap-API

# License

This product is licensed under the terms and conditions of EUPL-1.2. See [LICENSE] for further information.